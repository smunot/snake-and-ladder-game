package com.snakeandladdergame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

//@SpringBootApplication
public class SnakeAndLadderGameApplication {

    public static void main(String[] args) {
		PlayGame playGame = new PlayGame(100,4,4,1);
		Player player1 = new Player("A");
		Player player2 = new Player("B");
		Player player3 = new Player("C");
		Player player4 = new Player("D");

		playGame.addPlayer(player1);
		playGame.addPlayer(player2);
		playGame.addPlayer(player3);
		playGame.addPlayer(player4);

		String lost = playGame.play();
		System.out.println(lost + " lost the game");
//		SpringApplication.run(SnakeAndLadderGameApplication.class, args);
    }

}
