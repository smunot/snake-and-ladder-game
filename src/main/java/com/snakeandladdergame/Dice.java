package com.snakeandladdergame;

import java.util.concurrent.ThreadLocalRandom;

public class Dice {
    private int dices;

    public Dice(int dices) {
        this.dices = dices;
    }

    public int roll() {
        return ThreadLocalRandom.current().nextInt(6 * this.dices) + 1;
    }
}
