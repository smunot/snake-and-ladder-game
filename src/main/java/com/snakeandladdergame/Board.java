package com.snakeandladdergame;

public final class Board {
    private final int start;
    private final int end;

    public Board(int end){
        this.end = end;
        this.start = 0;
    }
    public int getEnd() {
        return end;
    }

}
