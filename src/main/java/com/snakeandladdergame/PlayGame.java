package com.snakeandladdergame;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class PlayGame {

    int numberOfSnakes;
    int numberOfLadders;

    Queue<Player> players;
    List<Snake> snakes;
    List<Ladder> ladders;

    Board board;
    Dice dice;

    public PlayGame(int boardSize, int numberOfSnakes, int numberOfLadders, int numberOfDice) {
        this.numberOfLadders = numberOfLadders;
        this.numberOfSnakes = numberOfSnakes;
        players = new ArrayDeque<>();
        ladders = new ArrayList<>();
        snakes = new ArrayList<>();
        dice = new Dice(numberOfDice);
        board = new Board(boardSize);
        initBoard();
    }

    private void initBoard() {
        Set<String> slSet = new HashSet<>();
        for (int i = 0; i < this.numberOfSnakes; i++) {
            while (true) {
                int head = ThreadLocalRandom.current().nextInt(this.board.getEnd()) + 1;
                int tail = ThreadLocalRandom.current().nextInt(this.board.getEnd()) + 1;
                if (head <= tail)
                    continue;
                String headTail = String.valueOf(head) + tail;
                if (slSet.contains(headTail))
                    continue;
                Snake snake = new Snake(head, tail);
                this.snakes.add(snake);
                slSet.add(headTail);
                break;
            }
        }
        for (int i = 0; i < this.numberOfLadders; i++) {
            while (true) {
                int start = ThreadLocalRandom.current().nextInt(this.board.getEnd()) + 1;
                int end = ThreadLocalRandom.current().nextInt(this.board.getEnd()) + 1;
                if (start >= end)
                    continue;
                String startEnd = String.valueOf(start) + end;
                if (slSet.contains(startEnd))
                    continue;
                Ladder ladder = new Ladder(start, end);
                this.ladders.add(ladder);
                slSet.add(startEnd);
                break;
            }
        }
    }

    public void addPlayer(Player player) {
        players.add(player);
    }

    public String play() {
        while (true) {
            Player player = players.poll();

            int newPosition = player.getCurrentPosition() + dice.roll();
            if (newPosition >= board.getEnd()) {
                System.out.println(player.getName() + " won the game");
            } else {
                players.add(getNewPosition(player, newPosition));
            }
            if (this.players.size() == 1) {
                break;
            }

        }
        return this.players.poll().getName();
    }

    private Player getNewPosition(Player player, int newPosition) {
        Snake snake = snakes.stream().filter(s -> s.getHead() == newPosition).findFirst().orElse(null);

        Ladder ladder = ladders.stream().filter(l -> l.getStart() == newPosition).findFirst().orElse(null);
        if (snake != null) {
            System.out.println(player.getName() + " got the snake head at " + newPosition + " shifted back at " + snake.getTail());
            player.setCurrentPosition(snake.getTail());
        } else if (ladder != null) {
            System.out.println(player.getName() + " got the ladder start at " + newPosition + " shifted at " + ladder.getEnd());
            player.setCurrentPosition(ladder.getEnd());
        } else {
            System.out.println(player.getName() + " moved forward by " + (newPosition - player.getCurrentPosition()));
            player.setCurrentPosition(newPosition);
        }
        return player;
    }
}
