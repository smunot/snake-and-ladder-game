package com.snakeandladdergame;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SnakeAndLadderGameApplicationTests {

	@Test
	void contextLoads() {
		PlayGame playGame = new PlayGame(100,4,4,1);
		Player player1 = new Player("Shailesh");
		Player player2 = new Player("Neha");
		Player player3 = new Player("Vaibhav");
		Player player4 = new Player("Gaurav");

		playGame.addPlayer(player1);
		playGame.addPlayer(player2);
		playGame.addPlayer(player3);
		playGame.addPlayer(player4);

		String lost = playGame.play();
		System.out.println(lost + " Loassed game");
	}

}


